using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
	[SerializeField] private float speed = 1f;
	[SerializeField] private Animator animator;

	private Rigidbody2D rb;
	private float horizontalDirection;
	private bool isDead = false;
	void Awake()
	{
		rb = GetComponent<Rigidbody2D>();
		horizontalDirection = Mathf.Sign(transform.localScale.x);
	}

	private void Update()
	{
		Move();
	}

	void Move()
	{
		float moveBy = horizontalDirection * speed;
		rb.velocity = new Vector2(moveBy, rb.velocity.y);
	}

	private void Flip()
	{
		horizontalDirection *= -1;

		// Flip game object in x
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	private void Die()
	{
		Debug.Log("Enemy dies");
		isDead = true;
		animator.SetBool("IsDead", true);
		GetComponent<Collider>().enabled = false;
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		// Detect direction of contact in x
		ContactPoint2D contact = other.GetContact(0);
		float collisionDirectionX = contact.normal.x > 0 ? -1 : 1;

		// Contact point and direction are the same
		if (Mathf.Sign(horizontalDirection * collisionDirectionX) > 0)
		{
			Flip();
		}

		if (!isDead && other.gameObject.CompareTag("Player"))
		{
			// Detect direction of contact in y
			float collisionDirectionY = contact.normal.y > 0 ? -1 : 1;

			if (collisionDirectionY == 1)
			{
				// Collided from top
				Die();
			}
		}
	}
}
