using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

enum GameState
{
	Start,
	Playing,
	Paused,
	Won,
	Lost
}

public class GameplayManager : MonoBehaviour
{
	[Header("Game options")]
	[SerializeField] private int maxLives = 3;

	[Header("Configuration")]
	[SerializeField] private PlayerController player;
	[SerializeField] private SwitchController doorSwitch;
	[SerializeField] private GameObject closedDoor;
	[SerializeField] private GameObject openDoor;
	[SerializeField] private GameObject canvas;
	[SerializeField] private GameObject uiCoinsCount;
	[SerializeField] private GameObject uiStatsCoinsCount;
	[SerializeField] private GameObject uiStatsLivesCount;
	[SerializeField] private GameObject uiStatsTimeCount;
	[SerializeField] private GameObject startOverlay;
	[SerializeField] private GameObject pauseOverlay;
	[SerializeField] private GameObject lostOverlay;
	[SerializeField] private GameObject wonOverlay;
	[SerializeField] private AudioSource audioLost;
	[SerializeField] private AudioSource audioWon;
	[SerializeField] private AudioSource audioUI;

	private GameState state;
	private bool isPaused = false;
	private int coinsCount = 0;
	private float startTime;
	private List<GameObject> livesUI = new List<GameObject>();

	void Awake()
	{
		// Populate config 
		player.lives = maxLives;

		// Populate actions
		player.OnKill += KillPlayer;
		player.OnHit += HitPlayer;
		doorSwitch.OnPressed += OpenDoor;
		openDoor.GetComponent<OpenDoorController>().OnEnter += WinLevel;
		GameObject[] collectibles = GameObject.FindGameObjectsWithTag("Coin");
		foreach (GameObject collectible in collectibles)
		{
			collectible.GetComponent<CollectibleController>().OnCollected += AddCoin;
		}
	}

	void Start()
	{
		DrawUILives();
		StateStart();
	}

	private void Update()
	{
		if (Input.anyKey)
		{
			if (state == GameState.Start)
			{
				StatePlaying();
				audioUI.Play();
			}

		}

		if (Input.GetKeyDown("escape"))
		{
			if (state == GameState.Playing)
			{
				StatePause();
				audioUI.Play();
			}
			else if (state == GameState.Paused)
			{
				StatePlaying();
				audioUI.Play();
			}
			else if (state == GameState.Lost || state == GameState.Won)
			{
				RestartLevel();
				audioUI.Play();
			}

		}

	}

	private void StateStart()
	{
		Pause(true);
		state = GameState.Start;
		startTime = Time.time;
		UpdateUI();
	}

	private void StatePlaying()
	{
		Pause(false);
		state = GameState.Playing;
		UpdateUI();
	}

	private void StatePause()
	{
		Pause(true);
		state = GameState.Paused;
		UpdateUI();
	}

	private void StateLost()
	{
		Pause(true);
		state = GameState.Lost;
		audioLost.Play();
		UpdateUI();
	}

	private void StateWon()
	{
		Pause(true);
		state = GameState.Won;
		audioWon.Play();
		UpdateUI();
	}

	private void Pause(bool pause)
	{
		Time.timeScale = pause ? 0f : 1f;
		isPaused = pause;
	}

	private void RestartLevel()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	private void WinLevel()
	{
		StateWon();
	}

	private void OpenDoor()
	{
		closedDoor.SetActive(false);
		openDoor.SetActive(true);
	}

	private void AddCoin()
	{
		coinsCount += 1;
		UpdateUI();
	}

	private void HitPlayer()
	{
		Debug.Log("hit " + player.lives);
		UpdateUI();
	}

	private void KillPlayer()
	{
		Invoke("StateLost", .1f);
	}

	private void UpdateUI()
	{
		// Draw coins
		uiCoinsCount.GetComponent<Text>().text = $"{coinsCount}";

		// Draw lives
		int i = 0;
		Sprite sprite;
		foreach (GameObject life in livesUI)
		{
			if (i < player.lives)
			{
				sprite = Resources.Load<Sprite>("Sprites/hudHeart_full");
			}
			else
			{
				sprite = Resources.Load<Sprite>("Sprites/hudHeart_empty");

			}
			life.GetComponent<Image>().sprite = sprite;
			i++;
		}

		// Draw stats
		float timer = Time.time - startTime;
		float minutes = Mathf.Floor(timer / 60);
		float seconds = Mathf.RoundToInt(timer % 60);
		uiStatsCoinsCount.GetComponent<Text>().text = $"{coinsCount}";
		uiStatsLivesCount.GetComponent<Text>().text = $"{player.lives}";
		uiStatsTimeCount.GetComponent<Text>().text = $"{minutes} min {seconds} sec";

		// Show overlays
		startOverlay.SetActive(state == GameState.Start);
		pauseOverlay.SetActive(state == GameState.Paused);
		lostOverlay.SetActive(state == GameState.Lost);
		wonOverlay.SetActive(state == GameState.Won);
	}

	private void DrawUILives()
	{
		const float y = 190f;
		const float x = 360f;
		const float scale = .4f;
		const float distance = 35f;

		for (int i = 0; i < maxLives; i++)
		{
			// Generate UI image in canvas
			GameObject obj = new GameObject($"Life ({i})");
			obj.AddComponent<Image>();
			obj.transform.SetParent(canvas.transform);

			// Move to position
			obj.GetComponent<RectTransform>().localPosition = new Vector2(x - i * distance, y);
			obj.transform.localScale = new Vector2(scale, scale);

			livesUI.Add(obj);
		}
	}
}
