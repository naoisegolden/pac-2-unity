using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleController : MonoBehaviour
{
	[SerializeField] private Animator animator;
	[SerializeField] private AudioSource audio;

	public Action OnCollected;

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
			OnCollected?.Invoke();

			animator.SetBool("IsCollected", true);
			audio.Play();
			Destroy(gameObject, .5f); // TODO: do callback in animation end
		}
	}
}
