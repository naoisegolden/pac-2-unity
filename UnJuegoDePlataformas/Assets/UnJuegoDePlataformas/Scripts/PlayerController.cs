using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public Action OnKill;
	public Action OnHit;
	[HideInInspector] public int lives = 3;

	private Rigidbody2D rb;
	private bool isGrounded = false;
	private bool isFacingRight = true;
	private bool canMove = true;
	private float horizontalMove;

	[SerializeField] private float speed = 5f;
	[SerializeField] private float jumpPower = 5f;
	[SerializeField] private float hitPower = 2.5f;
	[SerializeField] private Animator animator;
	[SerializeField] private AudioSource audioJump;
	[SerializeField] private AudioSource audioHit;

	private void Awake()
	{
		rb = GetComponent<Rigidbody2D>();
	}

	void Update()
	{
		if (canMove)
		{
			Move();
		}
		if (Input.GetButtonDown("Jump") && isGrounded == true && canMove)
		{
			Jump();
		}
	}
	void Move()
	{
		horizontalMove = Input.GetAxisRaw("Horizontal");
		float moveBy = horizontalMove * speed;
		rb.velocity = new Vector2(moveBy, rb.velocity.y);

		animator.SetFloat("Speed", Mathf.Abs(moveBy));

		if (moveBy > 0 && !isFacingRight)
		{
			Flip();
		}
		else if (moveBy < 0 && isFacingRight)
		{
			Flip();
		}
	}

	void Jump()
	{
		rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
		isGrounded = false;
		animator.SetBool("IsJumping", true);
		audioJump.Play();
	}

	private void Flip()
	{
		isFacingRight = !isFacingRight;

		// Flip game object in x
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	private void Freeze()
	{
		rb.constraints = RigidbodyConstraints2D.FreezePosition;
	}

	private void StopMoving()
	{
		canMove = false;
	}
	private void ResumeMoving()
	{
		canMove = true;
	}

	private void AnimateSetHit()
	{
		animator.SetBool("IsHit", true);
	}
	private void AnimateUnsetHit()
	{
		animator.SetBool("IsHit", false);
	}

	private void Hit()
	{
		const float AnimationDelay = .5f;

		// Remove a life
		lives -= 1;

		// Jump backwards
		StopMoving();
		rb.AddForce(Vector2.up * hitPower, ForceMode2D.Impulse);
		rb.AddForce(new Vector2(-horizontalMove * hitPower, 0f), ForceMode2D.Impulse);
		Invoke("ResumeMoving", AnimationDelay);

		// Animate
		AnimateSetHit();
		Invoke("AnimateUnsetHit", AnimationDelay);

		audioHit.Play();
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		// End jump
		isGrounded = true;
		animator.SetBool("IsJumping", false);

		if (other.gameObject.CompareTag("Enemy"))
		{
			Hit();
			OnHit?.Invoke();

			if (lives <= 0)
			{
				OnKill?.Invoke();
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Killer"))
		{
			OnKill?.Invoke();
		}

	}
}
