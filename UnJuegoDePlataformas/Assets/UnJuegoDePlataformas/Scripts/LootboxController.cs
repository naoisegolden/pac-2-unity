using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootboxController : MonoBehaviour
{
	[SerializeField] private Animator animator;
	[SerializeField] private AudioSource audio;

	public Action OnOpened;

	private bool isEmpty = false;

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (!isEmpty && other.gameObject.CompareTag("Player"))
		{
			// Detect direction of contact in y
			ContactPoint2D contact = other.GetContact(0);
			float collisionDirection = contact.normal.y > 0 ? -1 : 1;

			if (collisionDirection == -1)
			{
				// Collided from beneath
				animator.SetBool("IsEmpty", true);
				isEmpty = true;
				animator.Play("Base Layer.Lootbox_Open");
				audio.Play();

				OnOpened?.Invoke();
			}

		}
	}
}
