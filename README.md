# PEC 2 - Un joc de plataformes

Video: https://youtu.be/mkHRodL8xFE

## Implementation 

I used Cinemachine for the camera to follow the player. I implemented first [the code from the material provided](https://gitlab.com/uoc_vg/prog2d/ed.2021-2/modulo-2/-/blob/master/docs/Soluciones.md#reto-6), but once I did this, I wanted to add a bit more of complexity and Cinemachine was the perfect choice. 

Cinemachine not only tracks the game object you tell it, but also allows to define "damping" so that the camera movement is smoother, and boundary limits to which confine the camera movement. 

The scripts are generic enough to allow for more types of collectibles, enemies and killer colliders (like lava). For example CollectibleController is only used in coin but named collectible so it can be reused in other collectible items.

The lives are dynamic and can be set in the GameManager component. The UI is generated programmatically with UI Images and sprites.

These are all the scripts: 

- GameManager to control the game mechanics, points and score.
- PlayerController to control player movement and interaction with enemies and killers that either remove a life or kill.
- EnemyController to control enemies movement.
- CollectibleController to control collision with player, animate and update score.
- SwitchController to control interaction with the switch which opens the door.
- OpenDoorController to change display of the door and collision with the player.
- LootboxController to detect collision from player from beneath and animations.

## Architecture

I took some inspiration from this [How to architect game code](https://unity.com/how-to/architect-game-code-scriptable-objects) blog post. Although it talks about scriptable objects, which are not implemented, I did follow the recommendation of avoiding creating systems that have hard dependencies (e.g. using Actions) and that are interdependent. GameManager script knows about Player and Collectible, but they don't know about GameManager.

I used Action instead of SendMessage as a mean of communication between component script, because it allows to attach callbacks and to avoid hard dependencies.

Another architectural decision is detaching presentation from logic. Functions don't modify UI, only data. The `UpdateUI()` function is the single place where UI is modified.

## Gameplay



Use `a` or left arrow to move left, `d` or right arrow to move right. Space bar to jump.

Press `esc` at any time to pause the game.


## Sources

Images by Kenney (Freeware)
https://www.kenney.nl/assets/platformer-pack-redux

Font "Super Mario Bros" by Mario Monsters (Freeware)
https://www.fontspace.com/super-mario-bros-font-f3396

Sound effects from "Free SFX" by Kronbits (https://kronbits.itch.io/freesfx). CCO license (https://creativecommons.org/choose/zero/). 

Some sounds from the original Mario 1, from The Internet Archive
https://archive.org/details/mario_nes_snes_sounds/Mario+1+-+Game+Over.wav
